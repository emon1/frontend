import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { EnergyService } from '../services/energy.service';
import { TemperatureService } from '../services/temperature.service';
import { Energy } from 'src/app/models/energy.model'
import { Temperature } from 'src/app/models/temperature.model'
import { DatePipe } from '@angular/common';
import { StaticDataService, Tab } from '../services/tab.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent 
{
  tabs: Tab[];

  timespan = "Dag";
  totalEnergy = 1;
  totalCost = 0.024;
  mostRecent = "2020-03-24 15:33";
  mostRecentValue = 0.010;
  datePipe = new DatePipe('en-US');
  

  constructor(private breakpointObserver: BreakpointObserver, 
    public energyService: EnergyService, 
    public temperatureService: TemperatureService,
    tabService: StaticDataService) 
    {
      this.tabs = tabService.getTabs();
      this.totalEnergy = energyService.getTotalEnergy();
    }

    argumentCustomizeText(args: any) {
      const pipe = new DatePipe('en-US');
      return pipe.transform(args.value, 'M/d/yy, H:mm');
    }

    //Here goes the call to the backend to change the retrieved timeperiod
    clickTab(e) {
      this.timespan = e.itemData.text;

      this.energyService.setTimespan(e.itemData.id);
      this.temperatureService.setTimespan(e.itemData.id);
    }
}
