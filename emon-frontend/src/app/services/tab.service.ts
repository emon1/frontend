import { Injectable } from '@angular/core';

export class Tab {
	id: number;
	text: string;
}

const tabs: Tab[] = [
	{
		id: 0,
		text: 'Dag'
	},
	{
		id: 1,
		text: 'Week'
	}
];

@Injectable()
export class StaticDataService {
	getTabs(): Tab[] {
		return tabs;
	}
}
