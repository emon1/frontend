import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Temperature } from '../models/temperature.model';

@Injectable({
    providedIn: 'root',
  })
  export class TemperatureService {
  
    private temperatures: Temperature[];

    private timespans = ["day", "week"];
    private timeSpan = 0;

    private graphTemperatures
    private url = "http://94.130.56.173:3000/api/temperature"
    constructor(private http: HttpClient) { 
      this.fetch();
      setInterval(this.fetch.bind(this), 10000);
    }

    fetch() {
      console.log(this.temperatures);
      this.http.get<Temperature[]>(this.url + "/"  + this.timespans[this.timeSpan] + "/graph").subscribe(temperatures => {
        this.graphTemperatures = temperatures;
      } )
    }

    public setTimespan(id) 
    {
      this.timeSpan = id;
      this.fetch();
    }
 
    getTemperatures(): Temperature[]
    {
      return this.temperatures;
    }

    getTemperaturesGraph() : Temperature[]
    {
      return this.graphTemperatures;
    }
  }