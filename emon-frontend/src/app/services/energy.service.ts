import { Injectable } from '@angular/core'
import { Energy } from 'src/app/models/energy.model'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'

@Injectable({
    providedIn: 'root',
  })
  export class EnergyService {
    //Day == 0, Week == 1, Month == 2
    private timespans = ["day", "week"];
    private timeSpan = 0;

    private energies: Energy[];
    private graphEnergies: Energy[] = [];
  
    private totalPrice: number;
    private totalEnergy: number;

    private url = "http://94.130.56.173:3000/api/energy"
    constructor(private http: HttpClient) { 
      this.fetch();
      setInterval(this.fetch.bind(this), 20000);
    }

    fetch() {
      this.http.get<Energy[]>(this.url + "/"  + this.timespans[this.timeSpan] + "/graph").subscribe((energies) => {
        this.graphEnergies = energies;
      } )

      this.http.get<Energy[]>(this.url + "/"  + this.timespans[this.timeSpan] + "/graph").subscribe((energies) => {
        this.graphEnergies = energies;
      } )

      this.http.get<any>(this.url + "/"  + this.timespans[this.timeSpan] + "/total").subscribe((energy) => {
        this.totalEnergy = energy[0].kwh
      } )

      this.http.get<any>(this.url + "/"  + this.timespans[this.timeSpan] + "/price").subscribe((energy) => {
        this.totalPrice = energy.price
      })
    }

    public setTimespan(id) 
    {
      this.timeSpan = id;
      this.fetch();
    }

    getEnergies(): Energy[]
    {      
      return this.energies;
    }

    getEnergiesGraph() : Energy[]
    {
      return this.graphEnergies;
    }

    getTotalPrice(): number
    {
      return Math.round(this.totalPrice * 100) / 100;
    }

    getTotalEnergy(): number
    {
      return this.totalEnergy;
    }

    getLatest(): Energy
    {
      return this.graphEnergies[this.graphEnergies.length - 1];
    }
  }