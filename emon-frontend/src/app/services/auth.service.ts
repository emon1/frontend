import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user.model'
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root',
  })
export class AuthService {

    private url = "http://94.130.56.173:3000/api/auth"
    constructor(private http: HttpClient) 
    {

    }

    async login(username: string, password: string) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json")


        const httpOptions =  {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        }
        return this.http.post<User>(this.url + "/login", {username, password}, httpOptions).subscribe((user) => {
            console.log(user)
            localStorage.setItem("user", JSON.stringify(user));
        })
    } 

    async logout() {
        localStorage.removeItem('user');
    }

}