export interface Energy {
    _id: string;
    kwh: Number;
    date: Date;
}