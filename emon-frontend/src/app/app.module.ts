import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EnergyService } from './services/energy.service';
import { TemperatureService } from './services/temperature.service';
import { AuthService } from './services/auth.service';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner'; 
import {MatFormFieldModule} from '@angular/material/form-field';


import { DxChartModule, DxTabsModule } from 'devextreme-angular';
import { StaticDataService } from './services/tab.service';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input'
import { AppRoutingModule} from '../app/app-routing.module'
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { JwtMiddleware } from './middlewares/jwt.middleware';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    DashboardComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    HttpClientModule,
    DxTabsModule,
    DxChartModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    FormsModule,
    AppRoutingModule,
    MatSnackBarModule,
    MatInputModule,
    MatFormFieldModule
  ],
  providers: [EnergyService, TemperatureService, AuthService, StaticDataService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtMiddleware, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
