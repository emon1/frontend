import {NgModule}  from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from '../app/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavComponent } from './nav/nav.component'
import { AuthMiddleware } from './middlewares/auth.middleware'


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {path : '', component : NavComponent, canActivate: [AuthMiddleware]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }