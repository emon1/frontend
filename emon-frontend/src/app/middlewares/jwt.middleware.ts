import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class JwtMiddleware implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = JSON.parse(localStorage.getItem("user"));
        console.log(currentUser)
        if (currentUser && currentUser.token.token) {
            console.log("setting headers..")
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.token.token}`
                }
            });
        }

        return next.handle(request);
    }
}