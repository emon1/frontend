import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService} from '../services/auth.service'


@Injectable({ providedIn: 'root' })
export class AuthMiddleware implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        try{
            const token = JSON.parse(localStorage.getItem("user"));
        if(token.token) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
        }
        catch(err){
            this.router.navigate(['/login']);
            return false;
        }
        
    }
}