import { Component } from '@angular/core';
import { EnergyService } from './services/energy.service'
import { TemperatureService } from './services/temperature.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'EMON Project - Jordy van Raalte & Ian Vink';
  constructor() {  
    
  }
}
