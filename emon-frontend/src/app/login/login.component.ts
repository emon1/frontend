import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router'
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  constructor(private authService: AuthService, private router: Router, private _snackBar: MatSnackBar ) { }

  ngOnInit(): void {
    if(localStorage.getItem("user")) {
      console.log("navigating")
      this.router.navigate([""])
    } 
  }

  async login() {
    try {
      if(this.username && this.password){
        const user = await this.authService.login(this.username,this.password);
        if(localStorage.getItem("user")) {
          this.router.navigate([""])
        }else {
          //this._snackBar.open('Verkeerde gebruikersnaam of wachtwoord');
        }
      }
    } catch(error){
      this._snackBar.open('Verkeerde gebruikersnaam of wachtwoord');
      console.log(error);
    }
  }

}
